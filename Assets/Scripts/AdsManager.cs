﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour
{
    public void ShowRewardedAd()
    {
        Debug.Log("Showing Rewarded Ad");
        //check if the advertisement is ready
        //Show(rewardedVideo)

        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions
            {
                resultCallback = HandleShowResult
            };

            Advertisement.Show("rewardedVideo", options);
        }
    }

    void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //award 100 gems to player
                GameManager.Instance.Player.AddGems(100);
                UIManager.Instance.OpenShop(GameManager.Instance.Player.totalDiamonds);
                Debug.Log("You finished the add, here's 100 gems");
                break;
            case ShowResult.Skipped:
                Debug.Log("You skipped the add, no gems for you!");
                break;
            case ShowResult.Failed:
                Debug.Log("The video failed, it must have not been ready");
                break;
        }
    }
}
