﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    private bool _canDamage = true;
    private float _attackCooldown = 0.5f;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hit: " + other.name);

        IDamageable hit = other.GetComponent<IDamageable>();

        if(hit != null)
        {
            if (_canDamage)
            {
                hit.Damage();
                _canDamage = false;
                StartCoroutine(ResetAttackRoutine());
            }
        }
    }

    IEnumerator ResetAttackRoutine()
    {
        yield return new WaitForSeconds(_attackCooldown);
        _canDamage = true;
    }
}
