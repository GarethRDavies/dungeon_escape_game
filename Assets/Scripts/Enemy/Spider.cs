﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : Enemy, IDamageable
{
    public int Health { get; set; }
    public GameObject acidEffect;

    //use for initialisation
    public override void Init()
    {
        base.Init();

        Health = base.health;
    }

    public override void Update()
    {
        
    }

    public void Damage()
    {
        if (isDead)
        {
            return;
        }

        Health -= 1;
        if (Health < 1)
        {
            isDead = true;
            anim.SetTrigger("Death");
            GameObject diamond = Instantiate(diamondPrefab, transform.position, Quaternion.identity);
            diamond.GetComponent<Diamond>().gems = gems;
        }
    }

    public override void Movement()
    {
        //sit still
    }

    public void Attack()
    {
        Instantiate(acidEffect, transform.position, Quaternion.identity);
    }
}
