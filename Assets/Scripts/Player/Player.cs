﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour, IDamageable
{
    //var for amount of diamonds

    private Rigidbody2D _rigid;
    private PlayerAnimation _playerAnim;
    private SpriteRenderer _playerSprite;
    private SpriteRenderer _swordArcSprite;

    [SerializeField] private float horizontalSpeed = 3f;

    [SerializeField] private float _jumpSpeed = 5f;                                 //force for jumping
    [SerializeField] private float _groundedTolerance = 0.6f;                       //The vertical distance of the raycast used to calculate whether the player is grounded
    [SerializeField] private float _jumpCooldown = 0.1f;                             //The time after jumping where you are unable to jump
    private bool _resetJump = false;
    private bool _grounded = false;
    private bool _isDead = false;

    public int Health { get; set; }
    public int totalDiamonds;

	void Start ()
    {
        _rigid = GetComponent<Rigidbody2D>();
        _playerAnim = GetComponent<PlayerAnimation>();
        _playerSprite = GetComponentInChildren<SpriteRenderer>();
        _swordArcSprite = transform.GetChild(1).GetComponent<SpriteRenderer>();
        Health = 4;
	}

    void Update()
    {
        if (!_isDead)
        { 
            Movement();

            if (CrossPlatformInputManager.GetButtonDown("A_Button") && IsGrounded())
            {
                _playerAnim.Attack();
            }
        }   
	}

    private void Movement()
    {
        _grounded = IsGrounded();
        //Horizontal movement
        float horizontalInput = CrossPlatformInputManager.GetAxis("Horizontal");
        //if move >0 facing right
        if (horizontalInput > 0)                                                                //flip
        {
            Flip(true);
        }
        else if (horizontalInput < 0)
        {
            Flip(false);
        }

        //else if move < 0 facing left

        //Jumping
        if((Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetButtonDown("B_Button")) && IsGrounded())
        {
            //Debug.Log("Jump");
            _rigid.velocity = new Vector2(_rigid.velocity.x, _jumpSpeed);
            StartCoroutine(ResetJumpRoutine());
            //tell animator to jump
            _playerAnim.Jump(true);
        }

        _rigid.velocity = new Vector2(horizontalInput * horizontalSpeed, _rigid.velocity.y);
        _playerAnim.Move(horizontalInput);
    }

    bool IsGrounded()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, _groundedTolerance, 1 << 8);
        if (hitInfo.collider != null)
        {
            if (_resetJump == false)
            {
                //tell animator jump is false
                _playerAnim.Jump(false);
                return true;
            }
        }

        return false;
    }

    void Flip(bool faceRight)
    {
        if (faceRight == true)
        {
            _playerSprite.flipX = false;
            _swordArcSprite.flipX = false;
            _swordArcSprite.flipY = false;

            Vector3 newPos = _swordArcSprite.transform.localPosition;
            newPos.x = 1.01f;
            _swordArcSprite.transform.localPosition = newPos;
        }
        else if (faceRight == false)
        {
            _playerSprite.flipX = true;
            _swordArcSprite.flipX = true;
            _swordArcSprite.flipY = true;

            Vector3 newPos = _swordArcSprite.transform.localPosition;
            newPos.x = -1.01f;
            _swordArcSprite.transform.localPosition = newPos;
        }
    }

    IEnumerator ResetJumpRoutine()
    {
        _resetJump = true;
        yield return new WaitForSeconds(_jumpCooldown);
        _resetJump = false;
    }

    public void Damage()
    {
        if(Health < 1)
        {
            return;
        }
        Debug.Log("Player damaged");
        //remove 1 health
        Health -= 1;
        //update UI display
        UIManager.Instance.UpdateLives(Health);
        //check for dead
        //play death anim
        if (Health < 1)
        {
            _playerAnim.Death();
            _isDead = true;
        }
    }

    public void AddGems(int amount)
    {
        totalDiamonds += amount;
        UIManager.Instance.UpdateGemCount(totalDiamonds);
    }
}
