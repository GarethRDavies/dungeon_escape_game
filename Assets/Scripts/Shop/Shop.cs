﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    private Player player;

    public GameObject shopPanel;
    public int currentSelectedItem;
    public int currentItemCost;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            player = other.GetComponent<Player>();

            if (player != null)
            {
                UIManager.Instance.OpenShop(player.totalDiamonds);
            }

            shopPanel.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            shopPanel.SetActive(false);
        }
    }

    public void SelectItem(int item)
    {
        //0 = flame sword
        //1 = boots of flight
        //2 = key to the castle
        Debug.Log("Select Item: " + item);

        //switch between item
        //case 0
        switch (item)
        {
            case 0: //flame sword
                UIManager.Instance.UpdateShopSelection(75);
                currentSelectedItem = 0;
                currentItemCost = 200;
                break;
            case 1: //boots
                UIManager.Instance.UpdateShopSelection(-35);
                currentSelectedItem = 1;
                currentItemCost = 400;
                break;
            case 2: //key
                UIManager.Instance.UpdateShopSelection(-145);
                currentSelectedItem = 2;
                currentItemCost = 100;
                break;
        }
    }

    public void BuyItem()
    {
        if(player.totalDiamonds >= currentItemCost)
        {
            //award item
            if(currentSelectedItem == 2)
            {
                GameManager.Instance.HasKeyToCastle = true;
            }

            player.AddGems(-currentItemCost);
            Debug.Log("Purchased " + currentSelectedItem);
            Debug.Log("Remaining Gems: " + player.totalDiamonds);
            shopPanel.SetActive(false);
        }
        else
        {
            Debug.Log("Not enough gems for item, you fat cunt, closing shop");
            shopPanel.SetActive(false);
        }
    }
}
