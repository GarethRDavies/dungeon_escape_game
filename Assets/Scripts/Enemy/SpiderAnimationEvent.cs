﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderAnimationEvent : MonoBehaviour
{
    private Spider spider;

    private void Start()
    {
        spider = GetComponentInParent<Spider>();
    }

    public void Fire()
    {
        //Tell spider to fire
        //Debug.Log("Spider Should Fire");
        //use handle to call attack method on spider

        spider.Attack();
    }
}
